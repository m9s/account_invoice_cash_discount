# The COPYRIGHT file at the top level of this repository
# contains the full copyright notices and license terms.
import copy
import datetime
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Equal, Eval, Not, In, Or
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction

__metaclass__ = PoolMeta

__all__ = [
    'PaymentTermCashDiscountLine',
    'PaymentTermType',
    'PaymentTerm',
    'Invoice',
    'InvoiceCashDiscountLine',
    'PayInvoiceAsk',
    'PayInvoice'
]

STATES = {
    'readonly': Not(Equal(Eval('state'), 'draft'))
}
_TYPES = [
    ('discount', 'Discount'),
    ('net', 'Net')
]


class PaymentTermCashDiscountLine(ModelSQL, ModelView):
    'Payment Term Cash Discount Line'
    __name__ = 'account.invoice.payment_term.cash_discount_line'
    _rec_name = 'type'

    type = fields.Selection(_TYPES, 'Type', required=True)
    days = fields.Integer('Number of Days', required=True)
    percentage = fields.Numeric(
        'Percentage', digits=(16, 8), help='In %',
        states={
            'invisible': Not(Equal(Eval('type'), 'discount')),
            'required': Equal(Eval('type'), 'discount')
        }, depends=['type'])
    payment_term = fields.Many2One(
        'account.invoice.payment_term', 'Payment Term')

    @classmethod
    def __setup__(cls):
        super(PaymentTermCashDiscountLine, cls).__setup__()
        cls._order.insert(0, ('days', 'ASC'))

    @staticmethod
    def default_type():
        return 'net'

    @classmethod
    def get_date(cls, line, date):
        return date + datetime.timedelta(days=line.days)

    @classmethod
    def get_amount(cls, line, amount):
        percentage = line.percentage if line.percentage else Decimal('0.0')
        return amount - amount * percentage * Decimal('0.01')


class PaymentTermType(ModelSQL, ModelView):
    'Payment Term Type'
    __name__ = 'account.invoice.payment_term.type'

    name = fields.Char('Name', translate=True, required=True)
    code = fields.Char('Code', required=True)

    @classmethod
    def __setup__(cls):
        super(PaymentTermType, cls).__setup__()
        cls._sql_constraints += [
            ('code_uniq', 'UNIQUE(code)', 'Code must be unique!'),
        ]
        cls._order.insert(0, ('name', 'ASC'))


class PaymentTerm:
    __name__ = 'account.invoice.payment_term'

    account_customer = fields.Many2One(
        'account.account', 'Account Cash Discount Allowed',
        domain=[('kind', '=', 'revenue')],
        states={
            'invisible': Not(Equal(Eval('type'), 'cash_discount')),
            'required': Equal(Eval('type'), 'cash_discount')
        }, depends=['type'])
    account_supplier = fields.Many2One(
        'account.account', 'Account Cash Discount Received',
        domain=[('kind', '=', 'expense')],
        states={
            'invisible': Not(Equal(Eval('type'), 'cash_discount')),
            'required': Equal(Eval('type'), 'cash_discount')
        }, depends=['type'])
    type = fields.Selection('get_type', 'Type', required=True)
    cash_discount_lines = fields.One2Many(
        'account.invoice.payment_term.cash_discount_line',
        'payment_term', 'Cash Discount Lines',
        states={
            'invisible': Not(Equal(Eval('type'), 'cash_discount')),
            'required': Equal(Eval('type'), 'cash_discount')
        }, depends=['type'])

    @classmethod
    def __setup__(cls):
        super(PaymentTerm, cls).__setup__()
        cls._error_messages.update({
            'one_net_type_line': 'There can only be one line of type "Net"!',
        })
        cls.lines = copy.copy(cls.lines)
        if not cls.lines.states:
            cls.lines.states = {
                'invisible': Not(Equal(Eval('type'), 'partial_payment'))}
        else:
            if 'invisible' not in cls.lines.states:
                cls.lines.states.update({
                    'invisible': Not(Equal(Eval('type'), 'partial_payment'))
                })
            else:
                cls.lines.states['invisible'] = Or(
                    Not(Equal(Eval('type'), 'partial_payment')),
                    cls.lines.states['invisible'])
        if 'type' not in cls.lines.depends:
            cls.lines.depends = copy.copy(cls.lines.depends)
            cls.lines.depends.append('type')

    @classmethod
    def validate(cls, payment_terms):
        super(PaymentTerm, cls).validate(payment_terms)
        for payment_term in payment_terms:
            payment_term.check_net_type_line()

    def check_net_type_line(self):
        net_lines = [
            line for line in self.cash_discount_lines if line.type == 'net'
        ]
        if len(net_lines) > 1:
            self.raise_user_error('one_net_type_line')
        return True

    def check_remainder(self):
        if not self.type == 'cash_discount':
            return super(PaymentTerm, self).check_remainder()

    @classmethod
    def get_type(cls):
        Type = Pool().get('account.invoice.payment_term.type')
        types = Type.search([])
        return [(x.code, x.name) for x in types]

    @staticmethod
    def default_type():
        return 'cash_discount'

    def compute(self, amount, currency, date=None):
        CashDiscountLine = Pool().get(
            'account.invoice.payment_term.cash_discount_line')
        Date = Pool().get('ir.date')

        res = []
        if self.type == 'cash_discount':
            if date is None:
                date = Date.today()
            args = [('payment_term', '=', self.id),
                    ('type', '=', 'net')]
            lines = CashDiscountLine.search(args, limit=1)

            if lines:
                # Because of the validation check_net_type_line there can be
                # only one line of type 'net'
                line = lines[0]
                value_date = CashDiscountLine.get_date(line, date)
                value = amount
                res.append((value_date, value))
        else:
            res = super(PaymentTerm, self).compute(amount, currency, date=date)
        return res


class Invoice:
    __name__ = 'account.invoice'

    cash_discount_lines = fields.One2Many(
        'account.invoice.cash_discount_line',
        'invoice', 'Cash Discount Lines')

    def create_move(self):
        CashDiscountLine = Pool().get(
            'account.invoice.payment_term.cash_discount_line')
        InvoiceCashDiscountLine = Pool().get(
            'account.invoice.cash_discount_line')

        move = super(Invoice, self).create_move()
        if self.payment_term.type == 'cash_discount':
            for line in self.payment_term.cash_discount_lines:
                date = CashDiscountLine.get_date(line, self.invoice_date)
                amount = CashDiscountLine.get_amount(line, self.total_amount)
                data = {
                    'type': line.type,
                    'date': date,
                    'amount': amount,
                    'percentage': line.percentage,
                    'invoice': self.id,
                }
                InvoiceCashDiscountLine.create([data])
        return move

    @classmethod
    def check_cashdiscount(
            cls, invoice, date, amount_cashdiscount=False,
            amount_payment=False):
        CashDiscountLine = Pool().get(
            'account.invoice.cash_discount_line')
        Currency = Pool().get('currency.currency')

        if not amount_cashdiscount:
            if not amount_payment:
                return False
            amount_cashdiscount = invoice.amount_to_pay - amount_payment

        args = [('invoice', '=', invoice.id),
                ('type', '=', 'discount')]
        cash_discount_lines = CashDiscountLine.search(
            args, order=[('date', 'ASC')])

        for line in cash_discount_lines:
            if line.date >= date:
                amount_due = Currency.round(invoice.currency, line.amount)
                if amount_due + amount_cashdiscount == invoice.amount_to_pay:
                    return line.percentage
        return False

    def create_move_cash_discount(
            self, amount, date, account, journal, description,
            percentage_cashdiscount, amount_second_currency=None,
            second_currency=None):
        '''
        Adds a cash_discount move to an invoice

        :param amount: the amount of the cash discount to be posted
        :param account: the account for the cashdiscount line
        :param journal: the journal id for the move
        :param date: the date of the move
        :param description: the description of the move
        :param amount_second_currency: the amount in the second currenry if one
        :param second_currency: the id of the second currency
        :return: the payment line
        '''
        pool = Pool()
        Move = pool.get('account.move')
        Period = pool.get('account.period')
        Currency = pool.get('currency.currency')

        lines = []

        lines.append({
            'description': description,
            'account': self.account.id,
            'party': self.party.id,
            'debit': Decimal('0.0'),
            'credit': amount,
            'amount_second_currency': amount_second_currency,
            'second_currency': second_currency,
        })

        if self.taxes:
            untaxed_amount = self.untaxed_amount
            for tax in self.taxes:
                with Transaction().set_context(date=self.currency_date):
                    tax_base = Currency.compute(
                        self.currency, tax.base, self.company.currency)
                    amount_base = tax_base * percentage_cashdiscount * \
                        Decimal('0.01')
                    amount_tax = amount_base * tax.tax.rate

                    amount_base_second_currency = Decimal('0.0')
                    amount_tax_second_currency = Decimal('0.0')

                    if amount_second_currency and second_currency:
                        amount_base_second_currency = Currency.compute(
                            self.company.currency, amount_base,
                            second_currency)
                        amount_tax_second_currency = Currency.compute(
                            self.company.currency, amount_tax,
                            second_currency.id)
                amount_base = Currency.round(
                    self.company.currency, amount_base)
                amount_tax = Currency.round(
                    self.company.currency, amount_tax)
                line = {
                    'description': description,
                    'account': account.id,
                    'debit': amount_base,
                    'credit': Decimal('0.0'),
                    'amount_second_currency': amount_base_second_currency,
                    'second_currency': second_currency,
                }
                untaxed_amount -= tax_base
                if tax.base_code:
                    line.update({'tax_lines': [('create', [{
                        'code': tax.base_code.id,
                        'amount': amount_base * -tax.base_sign
                    }])]})
                lines.append(line)
                line = {
                    'description': description,
                    'account': tax.account.id,
                    'debit': amount_tax,
                    'credit': Decimal('0.0'),
                    'amount_second_currency': amount_tax_second_currency,
                    'second_currency': second_currency,
                }
                if tax.tax_code:
                    line.update({'tax_lines': [('create', [{
                        'code': tax.tax_code.id,
                        'amount': amount_tax * -tax.tax_sign
                    }])]})
                lines.append(line)
            if untaxed_amount != Decimal('0'):
                untaxed_amount_base = (
                    untaxed_amount * percentage_cashdiscount * Decimal('0.01'))
                untaxed_amount_second_currency = Decimal('0.0')
                if amount_second_currency and second_currency:
                    untaxed_amount_second_currency = Currency.compute(
                        self.company.currency, untaxed_amount_base,
                        second_currency)
                lines.append({
                    'description': description,
                    'account': account.id,
                    'debit': untaxed_amount_base,
                    'credit': Decimal('0.0'),
                    'amount_second_currency': untaxed_amount_second_currency,
                    'second_currency': second_currency,
                })

        if self.type in ('out_invoice', 'in_credit_note'):
            if self.account == account:
                self.raise_user_error('same_debit_account')

        else:
            for line in lines:
                credit = line['credit']
                debit = line['debit']
                line['credit'] = debit
                line['debit'] = credit
            if self.account == account:
                self.raise_user_error('same_credit_account')

        period_id = Period.find(self.company.id, date=date)

        moves = Move.create([{
            'description': description,
            'journal': journal.id,
            'period': period_id,
            'date': date,
            'lines': [('create', lines)],
        }])

        Move.post(moves)
        move = moves.pop()

        for line in move.lines:
            if line.account.id == self.account.id:
                self.write([self], {
                    'payment_lines': [('add', [line.id])],
                })
                return line
        raise Exception('Missing account')

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['cash_discount_lines'] = False
        return super(Invoice, cls).copy(records, default=default)


class InvoiceCashDiscountLine(ModelSQL, ModelView):
    'Invoice Cash Discount Line'
    __name__ = 'account.invoice.cash_discount_line'
    _rec_name = 'type'

    type = fields.Selection(_TYPES, 'Type', required=True)
    date = fields.Date('Date', required=True)
    amount = fields.Numeric('Amount', digits=(16, 4))
    percentage = fields.Numeric('Percentage', digits=(16, 8))
    invoice = fields.Many2One('account.invoice', 'Invoice', required=True)

    @classmethod
    def __setup__(cls):
        super(InvoiceCashDiscountLine, cls).__setup__()
        cls._order.insert(0, ('date', 'ASC'))


class PayInvoiceAsk:
    __name__ = 'account.invoice.pay.ask'

    journal_cashdiscount = fields.Many2One(
        'account.journal', 'Cash Discount Journal',
        states={
            'invisible': Not(Equal(Eval('type'), 'cashdiscount')),
            'required': Equal(Eval('type'), 'cashdiscount')
        }, depends=['type'])
    account_cashdiscount = fields.Many2One(
        'account.account', 'Cash Discount Account',
        domain=[('kind', '!=', 'view'), ('company', '=', Eval('company'))],
        states={
            'invisible': Not(Equal(Eval('type'), 'cashdiscount')),
            'required': Equal(Eval('type'), 'cashdiscount')
        }, depends=['company', 'type'])
    amount_cashdiscount = fields.Numeric(
        'Cash Discount Amount',
        digits=(16, Eval('currency_digits_cashdiscount', 2)), readonly=True,
        depends=['currency_digits_cashdiscount', 'type'], states={
            'invisible': Not(Equal(Eval('type'), 'cashdiscount')),
        })
    currency_cashdiscount = fields.Many2One(
        'currency.currency', 'Cash Discount Currency', readonly=True,
        states={
            'invisible': Not(Equal(Eval('type'), 'cashdiscount')),
        }, depends=['type'])
    currency_digits_cashdiscount = fields.Integer(
        'Cash Discount Currency Digits', readonly=True)
    percentage_cashdiscount = fields.Numeric(
        'Percentage', digits=(16, 8), depends=['type'], readonly=True,
        states={
            'invisible': Not(Equal(Eval('type'), 'cashdiscount')),
        }, help='In %')

    @classmethod
    def __setup__(cls):
        super(PayInvoiceAsk, cls).__setup__()
        cls.type = copy.copy(cls.type)
        if ('cashdiscount', 'Cash Discount') not in cls.type.selection:
            cls.type.selection += [('cashdiscount', 'Cash Discount')]

        cls.lines = copy.copy(cls.lines)
        if not cls.lines.states:
            cls.lines.states = {}
        if 'invisible' not in cls.lines.states:
            cls.lines.states.update({
                'invisible': Not(
                    In(Eval('type'), ['write_off', 'cashdiscount'])
                )
            })
        else:
            cls.lines.states['invisible'] = Or(
                Not(Equal(Eval('type'), 'cashdiscount')),
                cls.lines.states['invisible'])
        if 'type' not in cls.lines.depends:
            cls.lines.depends = copy.copy(cls.lines.depends)
            cls.lines.depends.append('type')

        cls.payment_lines = copy.copy(cls.payment_lines)
        if not cls.payment_lines.states:
            cls.payment_lines.states = {}
        if 'invisible' not in cls.payment_lines.states:
            cls.payment_lines.states.update({
                'invisible': Not(
                    In(Eval('type'), ['write_off', 'cashdiscount'])
                )
            })
        else:
            cls.payment_lines.states['invisible'] = Or(
                Not(Equal(Eval('type'), 'cashdiscount')),
                cls.payment_lines.states['invisible'])
        if 'type' not in cls.payment_lines.depends:
            cls.payment_lines.depends = copy.copy(cls.payment_lines.depends)
            cls.payment_lines.depends.append('type')

    @fields.depends('type')
    def on_change_lines(self):
        res = super(PayInvoiceAsk, self).on_change_lines()
        if self.type and self.type == 'cashdiscount':
            res['amount_cashdiscount'] = res['amount_writeoff']
            res['amount_writeoff'] = None

        return res


class PayInvoice:
    __name__ = 'account.invoice.pay'

    def default_ask(self, fields):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        Currency = pool.get('currency.currency')

        default = super(PayInvoice, self).default_ask(fields)

        invoice = Invoice(Transaction().context['active_id'])
        if default['amount'] < invoice.amount_to_pay:
            with Transaction().set_context(date=invoice.currency_date):
                amount_cashdiscount = Currency.compute(
                    invoice.company.currency, default['amount_writeoff'],
                    Currency(default['currency']))
            percentage_cashdiscount = Invoice.check_cashdiscount(
                invoice, self.start.date,
                amount_cashdiscount=amount_cashdiscount)
            if percentage_cashdiscount:
                default['type'] = 'cashdiscount'
                default['percentage_cashdiscount'] = percentage_cashdiscount
            else:
                default['percentage_cashdiscount'] = (
                    amount_cashdiscount * 100 / invoice.amount_to_pay)

        if invoice.type in ('out_invoice', 'out_credit_note'):
            default['account_cashdiscount'] = (
                invoice.payment_term.account_customer.id)
        else:
            default['account_cashdiscount'] = (
                invoice.payment_term.account_supplier.id)
        default['amount_cashdiscount'] = default['amount_writeoff']
        default['currency_digits_cashdiscount'] = (
            default['currency_digits_writeoff'])
        default['currency_cashdiscount'] = default['currency_writeoff']
        return default

    def transition_pay(self):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        Currency = pool.get('currency.currency')
        MoveLine = pool.get('account.move.line')

        res = super(PayInvoice, self).transition_pay()

        if hasattr(self.ask, 'type') and self.ask.type == 'cashdiscount':
            invoice = Invoice(Transaction().context['active_id'])

            amount_cashdiscount_second_currency = 0
            cashdiscount_second_currency = None
            if self.start.currency.id != invoice.company.currency.id:
                with Transaction().set_context(date=invoice.currency_date):
                    amount_cashdiscount_second_currency = \
                        Currency.compute(
                            self.start.currency_cashdiscount,
                            self.ask.amount_cashdiscount,
                            invoice.currency.id)
                cashdiscount_second_currency = invoice.currency.id

            invoice.create_move_cash_discount(
                self.ask.amount_cashdiscount,
                self.start.date, self.ask.account_cashdiscount,
                self.start.journal, self.start.description,
                self.ask.percentage_cashdiscount,
                amount_cashdiscount_second_currency,
                cashdiscount_second_currency)

            lines = [
                line for line in invoice.lines_to_pay
                if not line.reconciliation
            ]
            lines += [
                l for l in invoice.payment_lines if not l.reconciliation
            ]
            if lines:
                MoveLine.reconcile(lines)
        return res
