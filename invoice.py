#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
import datetime
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pyson import Equal, Eval, Not, In, Or
from trytond.pool import Pool
from trytond.transaction import Transaction


STATES = {
    'readonly': Not(Equal(Eval('state'), 'draft'))
}
_TYPES = [
        ('discount', 'Discount'),
        ('net', 'Net')
        ]

class PaymentTermCashDiscountLine(ModelSQL, ModelView):
    'Payment Term Cash Discount Line'
    _description = __doc__
    _name = 'account.invoice.payment_term.cash_discount_line'
    _rec_name = 'type'

    type = fields.Selection(_TYPES, 'Type', required=True)
    days = fields.Integer('Number of Days', required=True)
    percentage = fields.Numeric('Percentage', digits=(16, 8), help='In %',
            states={
                'invisible': Not(Equal(Eval('type'), 'discount')),
                'required': Equal(Eval('type'), 'discount')
             }, depends=['type'])
    payment_term = fields.Many2One('account.invoice.payment_term',
                                   'Payment Term')

    def __init__(self):
        super(PaymentTermCashDiscountLine, self).__init__()
        self._order.insert(0, ('days', 'ASC'))

    def default_type(self):
        return 'net'

    def get_date(self, line, date):
        return date + datetime.timedelta(days=line.days)

    def get_amount(self, line, amount):
        return amount - amount * line.percentage * Decimal('0.01')

PaymentTermCashDiscountLine()


class PaymentTermType(ModelSQL, ModelView):
    'Payment Term Type'
    _description = __doc__
    _name = 'account.invoice.payment_term.type'

    name = fields.Char('Name', translate=True, loading='lazy', required=True)
    code = fields.Char('Code', required=True)

    def __init__(self):
        super(PaymentTermType, self).__init__()
        self._sql_constraints += [
            ('code_uniq', 'UNIQUE(code)', 'Code must be unique!'),
        ]
        self._order.insert(0, ('name', 'ASC'))

PaymentTermType()


class PaymentTerm(ModelSQL, ModelView):
    _name = 'account.invoice.payment_term'

    account_customer = fields.Many2One('account.account',
            'Account Cash Discount Allowed',
            domain=[('kind', '=', 'revenue')],
            states={
                'invisible': Not(Equal(Eval('type'), 'cash_discount')),
                'required': Equal(Eval('type'), 'cash_discount')
                }, depends=['type'])
    account_supplier = fields.Many2One('account.account',
            'Account Cash Discount Received',
            domain=[('kind', '=', 'expense')],
            states={
                'invisible': Not(Equal(Eval('type'), 'cash_discount')),
                'required': Equal(Eval('type'), 'cash_discount')
                }, depends=['type'])
    type = fields.Selection('get_type', 'Type', required=True)
    cash_discount_lines = fields.One2Many(
            'account.invoice.payment_term.cash_discount_line',
            'payment_term', 'Cash Discount Lines',
            states={
                'invisible': Not(Equal(Eval('type'), 'cash_discount')),
                'required': Equal(Eval('type'), 'cash_discount')
            }, depends=['type'])

    def __init__(self):
        super(PaymentTerm, self).__init__()
        self._constraints += [
            ('_check_net_type_line', 'one_net_type_line'),
        ]
        self._error_messages.update({
            'one_net_type_line': 'There can only be one line of type "Net"!',
            })
        self.lines = copy.copy(self.lines)
        if not self.lines.states:
            self.lines.states={
                    'invisible': Not(Equal(Eval('type'), 'partial_payment'))}
        else:
            if 'invisible' not in self.lines.states:
                self.lines.states.update({
                        'invisible': Not(Equal(Eval('type'), 'partial_payment'))
                        })
            else:
                self.lines.states['invisible'] = Or(Not(Equal(
                        Eval('type'), 'partial_payment')
                        ), self.lines.states['invisible'])
        if not 'type' in self.lines.depends:
            self.lines.depends = copy.copy(self.lines.depends)
            self.lines.depends.append('type')
        self._reset_columns()

    def _check_net_type_line(self, ids):
        payment_terms = self.browse(ids)
        res = True
        for payment_term in payment_terms:
            x = 0
            for line in payment_term.cash_discount_lines:
                if line.type=='net':
                    x += 1
            if x>1:
                res = False
        return res

    def get_type(self):
        type_obj = Pool().get('account.invoice.payment_term.type')
        type_ids = type_obj.search([])
        types = type_obj.browse(type_ids)
        return [(x.code, x.name) for x in types]

    def default_type(self):
        return 'cash_discount'

    def compute(self, amount, currency, payment_term, date=None):
        cash_discount_line_obj = Pool().get(
                           'account.invoice.payment_term.cash_discount_line')
        date_obj = Pool().get('ir.date')

        res = []
        if payment_term.type=='cash_discount':
            if date is None:
                date = date_obj.today()
            args = [('payment_term', '=', payment_term.id),
                    ('type', '=', 'net')]
            line_ids = cash_discount_line_obj.search(args, limit=1)

            if line_ids:
                line = cash_discount_line_obj.browse(line_ids[0])
                value_date = cash_discount_line_obj.get_date(line, date)
                value = amount
                res.append((value_date, value))
        else:
            res = super(PaymentTerm, self).compute(amount, currency,
                    payment_term, date=date)
        return res

PaymentTerm()


class Invoice(ModelSQL, ModelView):
    _name = 'account.invoice'

    cash_discount_lines = fields.One2Many(
                          'account.invoice.cash_discount_line',
                          'invoice', 'Cash Discount Lines')

    def create_move(self, invoice_id):
        cash_discount_line_obj = Pool().get(
                           'account.invoice.payment_term.cash_discount_line')
        invoice_cash_discount_line_obj = Pool().get(
                           'account.invoice.cash_discount_line')

        move_id = super(Invoice, self).create_move(invoice_id)
        invoice = self.browse(invoice_id)
        if invoice.payment_term.type=='cash_discount':
            for line in invoice.payment_term.cash_discount_lines:
                date = cash_discount_line_obj.get_date(line,
                        invoice.invoice_date)
                amount = cash_discount_line_obj.get_amount(line,
                        invoice.total_amount)
                data = {'type': line.type,
                        'date': date,
                        'amount': amount,
                        'percentage': line.percentage,
                        'invoice': invoice_id,
                        }
                invoice_cash_discount_line_obj.create(data)
        return move_id

    def check_cashdiscount(self, invoice_id, date, amount_cashdiscount=False,
            amount_payment=False):
        cash_discount_line_obj = Pool().get(
                'account.invoice.cash_discount_line')
        currency_obj = Pool().get('currency.currency')

        invoice = self.browse(invoice_id)

        if not amount_cashdiscount:
            if not amount_payment:
                return False
            amount_cashdiscount = invoice.amount_to_pay - amount_payment

        args = [('invoice', '=', invoice_id),
                ('type', '=', 'discount')]
        cash_discount_line_ids = cash_discount_line_obj.search(args,
                order=[('date','ASC')])
        cash_discount_lines = cash_discount_line_obj.browse(
                cash_discount_line_ids)
        for line in cash_discount_lines:
            if line.date >= date:
                amount_due = currency_obj.round(invoice.currency, line.amount)
                if amount_due + amount_cashdiscount == invoice.amount_to_pay:
                    return line.percentage
        return False

    def create_move_cash_discount(self, amount, invoice_id, date, account_id,
            journal_id, description, percentage_cashdiscount,
            amount_second_currency=False, second_currency=False):
        '''
        Adds a cash_discount move to an invoice

        :param invoice_id: the invoice id
        :param amount: the amount of the cash discount to be posted
        :param account_id: the account id for the cashdiscount line
        :param journal_id: the journal id for the move
        :param date: the date of the move
        :param description: the description of the move
        :param amount_second_currency: the amount in the second currenry if one
        :param second_currency: the id of the second currency
        :return: the id of the payment line
        '''
        pool = Pool()
        journal_obj = pool.get('account.journal')
        move_obj = pool.get('account.move')
        period_obj = pool.get('account.period')
        currency_obj = pool.get('currency.currency')

        lines = []
        invoice = self.browse(invoice_id)
        journal = journal_obj.browse(journal_id)

        if invoice.type in ('out_invoice', 'in_credit_note'):
            lines.append({
                'name': description,
                'account': invoice.account.id,
                'party': invoice.party.id,
                'debit': Decimal('0.0'),
                'credit': amount,
                'amount_second_currency': amount_second_currency,
                'second_currency': second_currency,
            })
            if invoice.taxes:
                for tax in invoice.taxes:
                    with Transaction().set_context(date=invoice.currency_date):
                        tax_base = currency_obj.compute(invoice.currency.id,
                            tax.base, invoice.company.currency.id)
                        amount_base = tax_base * percentage_cashdiscount * \
                                Decimal('0.01')
                        amount_tax = amount_base * tax.tax.percentage * \
                                Decimal('0.01')
                        amount_base_second_currency = False
                        amount_tax_second_currency = False
                        if amount_second_currency and second_currency:
                            amount_base_second_currency = currency_obj.compute(
                                    invoice.company.currency.id, amount_base,
                                    second_currency.id)
                            amount_tax_second_currency = currency_obj.compute(
                                    invoice.company.currency.id, amount_tax,
                                    second_currency.id)
                    amount_base = currency_obj.round(invoice.company.currency,
                            amount_base)
                    amount_tax = currency_obj.round(invoice.company.currency,
                            amount_tax)
                    line = {
                        'name': description,
                        'account': account_id,
                        'party': invoice.party.id,
                        'debit': amount_base,
                        'credit': Decimal('0.0'),
                        'amount_second_currency': amount_base_second_currency,
                        'second_currency': second_currency,
                        }
                    if tax.base_code:
                        line.update({'tax_lines': [('create', {
                                'code': tax.base_code.id,
                                'amount': amount_base * -tax.base_sign
                                })]})
                    lines.append(line)
                    line = {
                        'name': description,
                        'account': tax.account.id,
                        'party': invoice.party.id,
                        'debit': amount_tax,
                        'credit': Decimal('0.0'),
                        'amount_second_currency': amount_tax_second_currency,
                        'second_currency': second_currency,
                        }
                    if tax.tax_code:
                        line.update({'tax_lines': [('create', {
                                'code': tax.tax_code.id,
                                'amount': amount_tax * -tax.tax_sign
                                })]})
                    lines.append(line)
            else:
                lines.append({
                    'name': description,
                    'account': account_id,
                    'party': invoice.party.id,
                    'debit': amount,
                    'credit': Decimal('0.0'),
                    'amount_second_currency': amount_second_currency,
                    'second_currency': second_currency,
                })
            if invoice.account.id == account_id:
                self.raise_user_error('same_debit_account')
        else:
            lines.append({
                'name': description,
                'account': invoice.account.id,
                'party': invoice.party.id,
                'debit': amount,
                'credit': Decimal('0.0'),
                'amount_second_currency': amount_second_currency,
                'second_currency': second_currency,
            })
            if invoice.taxes:
                for tax in invoice.taxes:
                    amount_base = tax.base * percentage_cashdiscount * \
                            Decimal('0.01')
                    amount_tax = amount_base * tax.tax.percentage * \
                            Decimal('0.01')
                    if amount_second_currency and second_currency:
                        with Transaction().set_context(
                                date=invoice.currency_date):
                            amount_base_second_currency = currency_obj.compute(
                                invoice.company.currency.id, amount_base,
                                second_currency.id)
                            amount_tax_second_currency = currency_obj.compute(
                                invoice.company.currency.id, amount_tax,
                                second_currency.id)
                    line = {
                        'name': description,
                        'account': account_id,
                        'party': invoice.party.id,
                        'debit': Decimal('0.0'),
                        'credit': amount_base,
                        'amount_second_currency': amount_base_second_currency,
                        'second_currency': second_currency,
                        }
                    if tax.base_code:
                        line.update({'tax_lines': [('create', {
                                'code': tax.base_code.id,
                                'amount': amount_base * -tax.base_sign
                                })]})
                    lines.append(line)
                    line = {
                        'name': description,
                        'account': tax.account.id,
                        'party': invoice.party.id,
                        'debit': Decimal('0.0'),
                        'credit': amount_tax,
                        'amount_second_currency': amount_tax_second_currency,
                        'second_currency': second_currency,
                        }
                    if tax.tax_code:
                        line.update({'tax_lines': [('create', {
                                'code': tax.tax_code.id,
                                'amount': amount_tax * -tax.tax_sign
                                })]})
                    lines.append(line)
            else:
                lines.append({
                    'name': description,
                    'account': account_id,
                    'party': invoice.party.id,
                    'debit': Decimal('0.0'),
                    'credit': amount,
                    'amount_second_currency': amount_second_currency,
                    'second_currency': second_currency,
                })
            if invoice.account.id == account_id:
                self.raise_user_error('same_credit_account')

        period_id = period_obj.find(invoice.company.id, date=date)

        move_id = move_obj.create({
            'journal': journal.id,
            'period': period_id,
            'date': date,
            'lines': [('create', x) for x in lines],
            })

        move = move_obj.browse(move_id)

        for line in move.lines:
            if line.account.id == invoice.account.id:
                self.write(invoice.id, {
                    'payment_lines': [('add', line.id)],
                    })
                return line.id
        raise Exception('Missing account')

    def copy(self, ids, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['cash_discount_lines'] = False
        return super(Invoice, self).copy(ids, default=default)

Invoice()


class InvoiceCashDiscountLine(ModelSQL, ModelView):
    'Invoice Cash Discount Line'
    _description = __doc__
    _name = 'account.invoice.cash_discount_line'
    _rec_name = 'type'

    type = fields.Selection(_TYPES, 'Type', required=True)
    date = fields.Date('Date', required=True)
    amount = fields.Numeric('Amount', digits=(16, 4))
    percentage = fields.Numeric('Percentage', digits=(16, 8))
    invoice = fields.Many2One('account.invoice', 'Invoice', required=True)

    def __init__(self):
        super(InvoiceCashDiscountLine, self).__init__()
        self._order.insert(0, ('date', 'ASC'))

InvoiceCashDiscountLine()


class PayInvoiceAsk(ModelView):
    _name = 'account.invoice.pay_invoice.ask'

    def __init__(self):
        super(PayInvoiceAsk, self).__init__()
        self.type = copy.copy(self.type)
        if ('cashdiscount', 'Cash Discount') not in self.type.selection:
            self.type.selection += [('cashdiscount', 'Cash Discount')]

        self.lines = copy.copy(self.lines)
        if not self.lines.states:
            self.lines.states = {}
        if 'invisible' not in self.lines.states:
            self.lines.states.update({
                    'invisible': Not(
                        In(Eval('type'), ['write_off','cashdiscount'])
                        )
                    })
        else:
            self.lines.states['invisible'] = Or(Not(Equal(
                    Eval('type'), 'cashdiscount')
                    ), self.lines.states['invisible'])
        if not 'type' in self.lines.depends:
            self.lines.depends = copy.copy(self.lines.depends)
            self.lines.depends.append('type')
        if not self.lines.on_change:
            self.lines.on_change = []
        if 'type' not in self.lines.on_change:
            self.lines.on_change += ['type']

        # TODO: Check if really necessary
        self.payment_lines = copy.copy(self.payment_lines)
        if not self.payment_lines.states:
            self.payment_lines.states = {}
        if 'invisible' not in self.payment_lines.states:
            self.payment_lines.states.update({
                    'invisible': Not(
                        In(Eval('type'), ['write_off','cashdiscount'])
                        )
                    })
        else:
            self.payment_lines.states['invisible'] = Or(Not(Equal(
                    Eval('type'), 'cashdiscount')
                    ), self.payment_lines.states['invisible'])
        if not 'type' in self.payment_lines.depends:
            self.payment_lines.depends = copy.copy(self.payment_lines.depends)
            self.payment_lines.depends.append('type')

        self._reset_columns()

    journal_cashdiscount = fields.Many2One('account.journal',
            'Cash Discount Journal',
            states={
                'invisible': Not(Equal(Eval('type'), 'cashdiscount')),
                'required': Equal(Eval('type'), 'cashdiscount')
            }, depends=['type'])
    account_cashdiscount = fields.Many2One('account.account',
            'Cash Discount Account',
            domain=[('kind', '!=', 'view'), ('company', '=', Eval('company'))],
            states={
                'invisible': Not(Equal(Eval('type'), 'cashdiscount')),
                'required': Equal(Eval('type'), 'cashdiscount')
            }, depends=['company', 'type'])
    amount_cashdiscount = fields.Numeric('Cash Discount Amount',
            digits=(16, Eval('currency_digits_cashdiscount', 2)), readonly=True,
            depends=['currency_digits_cashdiscount', 'type'], states={
                'invisible': Not(Equal(Eval('type'), 'cashdiscount')),
                })
    currency_cashdiscount = fields.Many2One('currency.currency',
            'Cash Discount Currency', readonly=True, states={
                'invisible': Not(Equal(Eval('type'), 'cashdiscount')),
                }, depends=['type'])
    currency_digits_cashdiscount = fields.Integer(
            'Cash Discount Currency Digits', readonly=True)
    percentage_cashdiscount = fields.Numeric('Percentage', digits=(16, 8),
            help='In %')

    def on_change_lines(self, vals):
        res = super(PayInvoiceAsk, self).on_change_lines(vals)
        if vals.get('type') and vals['type']=='cashdiscount':
            res['amount_cashdiscount'] = res['amount_writeoff']
            res['amount_writeoff'] = False

        return res

PayInvoiceAsk()


class PayInvoice(Wizard):
    _name = 'account.invoice.pay_invoice'

    def _ask(self, data):
        invoice_obj = Pool().get('account.invoice')
        currency_obj = Pool().get('currency.currency')

        res = super(PayInvoice, self)._ask(data)

        invoice = invoice_obj.browse(data['id'])
        if res['amount'] < invoice.amount_to_pay:
            with Transaction().set_context(date=invoice.currency_date):
                amount_cashdiscount = currency_obj.compute(
                    invoice.company.currency.id, res['amount_writeoff'],
                    res['currency'])
            percentage_cashdiscount = invoice_obj.check_cashdiscount(
                    data['id'], res['date'],
                    amount_cashdiscount=amount_cashdiscount)
            if percentage_cashdiscount:
                res['type'] = 'cashdiscount'
                res['percentage_cashdiscount'] = percentage_cashdiscount
        if invoice.type in ('out_invoice', 'out_credit_note'):
            res['account_cashdiscount'] = \
                    invoice.payment_term.account_customer.id
        else:
            res['account_cashdiscount'] = \
                    invoice.payment_term.account_supplier.id
        res['amount_cashdiscount'] = res['amount_writeoff']
        res['currency_digits_cashdiscount' ] = res['currency_digits_writeoff']
        res['currency_cashdiscount'] = res['currency_writeoff']
        return res

    def _action_pay(self, data):
        pool = Pool()
        invoice_obj = pool.get('account.invoice')
        currency_obj = pool.get('currency.currency')
        move_line_obj = pool.get('account.move.line')

        invoice = invoice_obj.browse(data['id'])

        with Transaction().set_context(date=invoice.currency_date):
            amount = currency_obj.compute(data['form']['currency'],
                data['form']['amount'], invoice.company.currency.id)

        reconcile_lines = invoice_obj.get_reconcile_lines_for_amount(invoice,
                amount)

        amount_second_currency = False
        second_currency = False
        if data['form']['currency'] != invoice.company.currency.id:
            amount_second_currency = data['form']['amount']
            second_currency = data['form']['currency']

        if amount > invoice.amount_to_pay and \
                data['form'].get('type') != 'writeoff':
            self.raise_user_error('amount_greater_amount_to_pay')

        line_id = False
        cashdiscount_line_id = False
        if not currency_obj.is_zero(invoice.company.currency, amount):
            line_id = invoice_obj.pay_invoice(data['id'], amount,
                    data['form']['journal'], data['form']['date'],
                    data['form']['description'], amount_second_currency,
                    second_currency)
        if data['form'].get('type') == 'cashdiscount':
            amount_cashdiscount_second_currency = False
            cashdiscount_second_currency = False
            if data['form']['currency'] != invoice.company.currency.id:
                with Transaction().set_context(date=invoice.currency_date):
                    amount_cashdiscount_second_currency = \
                        currency_obj.compute(
                        data['form']['currency_cashdiscount'],
                        data['form']['amount_cashdiscount'],
                        invoice.currency.id)
                cashdiscount_second_currency = invoice.currency.id
            cashdiscount_line_id = invoice_obj.create_move_cash_discount(
                    data['form']['amount_cashdiscount'], invoice.id,
                    data['form']['date'], data['form']['account_cashdiscount'],
                    data['form']['journal'], data['form']['description'],
                    data['form']['percentage_cashdiscount'],
                    amount_cashdiscount_second_currency,
                    cashdiscount_second_currency)

        if reconcile_lines[1] != Decimal('0.0'):
            if data['form'].get('type') == 'writeoff':
                line_ids = data['form']['lines'][0][1] + \
                        [x.id for x in invoice.payment_lines
                                if not x.reconciliation]
                if line_id:
                    line_ids += [line_id]
                if cashdiscount_line_id:
                    line_ids += [cashdiscount_line_id]
                if line_ids:
                    move_line_obj.reconcile(line_ids,
                            journal_id=data['form']['journal_writeoff'],
                            date=data['form']['date'],
                            account_id=data['form']['account_writeoff'])
            elif data['form'].get('type') == 'cashdiscount':
                line_ids = data['form']['lines'][0][1] + \
                        [x.id for x in invoice.payment_lines
                                if not x.reconciliation]
                if line_id:
                    line_ids += [line_id]
                if cashdiscount_line_id:
                    line_ids += [cashdiscount_line_id]
                if line_ids:
                    move_line_obj.reconcile(line_ids)
        else:
            line_ids = reconcile_lines[0]
            if line_id:
                line_ids += [line_id]
            if cashdiscount_line_id:
                    line_ids += [cashdiscount_line_id]
            if line_ids:
                move_line_obj.reconcile(line_ids)
        return {}

PayInvoice()
