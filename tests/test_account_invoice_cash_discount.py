# The COPYRIGHT file at the top level of this repository
# contains the full copyright notices and license terms.
import os
import sys
DIR = os.path.abspath(os.path.normpath(os.path.join(
    __file__, '..', '..', '..', '..', '..', 'trytond')))
if os.path.isdir(DIR):
    sys.path.insert(0, os.path.dirname(DIR))

import unittest
import doctest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import test_view, test_depends, \
    doctest_setup, doctest_teardown


class AccountInvoiceCashDiscountTestCase(unittest.TestCase):
    '''
    Test AccountInvoiceCashDiscount module.
    '''

    def setUp(self):
        trytond.tests.test_tryton.install_module(
            'account_invoice_cash_discount')

    def test0005views(self):
        '''
        Test views.
        '''
        test_view('account_invoice_cash_discount')

    def test0006depends(self):
        '''
        Test depends.
        '''
        test_depends()


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        AccountInvoiceCashDiscountTestCase))
    suite.addTests(doctest.DocFileSuite(
        'scenario_invoice_cash_discount.rst',
        setUp=doctest_setup, tearDown=doctest_teardown, encoding='utf-8',
        optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
