#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Cash Discount',
    'name_de_DE': 'Fakturierung Skonto',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''Account Invoice Cash Discount
    - Allows the definition of cash discounts in payment terms
''',
    'description_de_DE': '''Fakturierung Skonto
    - Ermöglicht die Definition von Skontobedingungen in Zahlungsbedingungen.
''',
    'depends': [
        'account_invoice',
    ],
    'xml': [
        'invoice.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
