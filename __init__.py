# The COPYRIGHT file at the top level of this repository
# contains the full copyright notices and license terms.

from trytond.pool import Pool
from invoice import *


def register():
    Pool.register(
        PaymentTermCashDiscountLine,
        PaymentTermType,
        PaymentTerm,
        Invoice,
        InvoiceCashDiscountLine,
        PayInvoiceAsk,
        module='account_invoice_cash_discount', type_='model')
    Pool.register(
        PayInvoice,
        module='account_invoice_cash_discount', type_='wizard')
